# README #

### What is this repository for? ###
 Using libuvc library for streaming usb cameras.

### How do I get set up? ###
 Prerequisites: Libuvc, Libusb and CMake.
 
### Terminal command to execute the code: <br>

 Compilation: g++ testuvc.cpp -o testoutput -luvc ` pkg-config --cflags --libs opencv4` <br>
 Output: sudo ./testoutput
 